"use strict";
jQuery(document).ready(function($){
  $(".btn__main-menu").click(function(){
    $('.navbar-nav').toggleClass('active');
  })

  $('.footer__tag h3').click(function(){

    if($('.footer__tag ul').hasClass('active')){
      $('.footer__tag ul').removeClass('active');
    }
    $(this).next('ul').toggleClass('active');
  });
  
  //click ra bên ngoài popup
  // $(document).click(function(e){
  //   if($(e.target).closest("#abc").attr("id")!="abc"){
  //     $('.footer__tag ul').removeClass('active');
  //   }
  // })
 
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 9000000,12000000],
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.values[ 0 ] + "VNĐ - " + ui.values[ 1 ] + " VNĐ");
      }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) +
      " VNĐ - " + $( "#slider-range" ).slider( "values", 1 ) + " VNĐ");
  

  closEst("#abc","abc",".footer__tag ul","active");
  $(window).on('load', function(){
  });
  $('.product-zoom .owl-carousel').owlCarousel({
    loop:true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    margin:10,
    nav:false,
    dots:false,
    items:1
  })
  $('.list-url--product .owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
      0:{
          items:4
      },
      600:{
          items:4
      },
      1000:{
          items:4
      }
    }
  })
  if ($('.list-url--product .item').length != 0) {
    $('.list-url--product .item').click(function(){
      $(this).closest('.list-url--product').find('.item').removeClass('active');      $(this).addClass('active');
    })
  }
  if ($('#number-product .quantity-box').length != 0) {
    $('#number-product .add').click(function(){
      if($('#number-input').val() > 0){
        $('#number-input').val(+ $('#number-input').val() + 1);
      }
    });
    $('#number-product .sub').click(function(){
      if($('#number-input').val() > 1){
        $('#number-input').val(+ $('#number-input').val() - 1);
      }
    });
  }
  if ($('#number-product .like-box').length != 0) {
    $('#number-product .like-box button').click(function(){
      $(this).toggleClass('active');
    });  
  }
  $("#contact-form").validate({
    rules: {
      fullname: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      number:{
        required: true,
        number: true,
        minlength : 6
      }
    },
    messages:{
      fullname: {
        required: "Vui lòng nhập họ tên"
      },
      email: {
        required: "Vui lòng nhập Email",
        email: "Nhập đúng email"
      },
      number:{
        required: "Vui lòng nhập số điện thoại",
        number: "Nhập đúng số điện thoại",
        minlength : "Nhập đúng số điện thoại"
      }
    }
  });
  $('.sanpham-col .library-content ul li a').click(function(){
    $(this).closest(".library-content ul li").find(".minus").toggleClass('hide');
    $(this).closest(".library-content ul li").find(".plus").toggleClass('hide');
    
  })
  $('body').click(function(e){
    if($(e.target).closest('.sanpham-col .library-content ul li a').length === 0){
      $(".library-content ul li .minus").addClass('hide');
      $(".library-content ul li .plus").removeClass('hide');
    }
  })
});
